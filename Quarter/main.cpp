#include <cstdlib>
#include <string>
#include <iostream>

using namespace std;

int main(int ac, char* av[]) {
	// Check number of arguments
	if (ac != 2) {
		cerr << "Incorrect number of arguments given. Expected one number (1-12)." << endl;
		exit(EXIT_FAILURE);
	}

	// Load argument
	string arg = av[1];

	try {
		// Convert argument to integer
		int m = stoi(arg);

		// Check scope
		if (m > 0 && m < 13) {
			// Calculate quarter
			int q = (m - 1) / 3 + 1;
			cout << "Q" << q << endl;
		}
		else {
			// Month doesn't exist
			cerr << "The number you entered is out of range." << endl;
			exit(EXIT_FAILURE);
		}
	}
	catch (out_of_range&) {
		// Integer given, but out of range
		cerr << "The number you entered is out of range." << endl;
		exit(EXIT_FAILURE);
	}
	catch (invalid_argument&) {
		// String given
		cerr << "The argument you entered is not a number." << endl;
		exit(EXIT_FAILURE);
	}

	return EXIT_SUCCESS;
}
